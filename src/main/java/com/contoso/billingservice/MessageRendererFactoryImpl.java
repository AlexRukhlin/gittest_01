package com.contoso.billingservice;

public class MessageRendererFactoryImpl
    implements MessageRendererFactory
{
    public MessageRenderer getDefaultMessageRenderer()
    {
        return new ConsoleMessageRenderer();
    }
}
