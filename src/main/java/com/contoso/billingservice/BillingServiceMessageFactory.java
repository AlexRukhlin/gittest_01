package com.contoso.billingservice;

/**
 * Interface to be implemented by BillingServiceMessageFactory implementations. 
 */
public interface BillingServiceMessageFactory
{
    String getHelloWorldMessage(MessageLength length);
}