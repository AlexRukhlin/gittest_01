package com.contoso.billingservice;

/**
 * A simple classic Hello World application.
 * master
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( getMessage() );
    }
    
    public static String getMessage()
    {
    	return "Hello World!";
    }
}
