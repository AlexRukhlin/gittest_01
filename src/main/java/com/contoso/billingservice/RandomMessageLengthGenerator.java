package com.contoso.billingservice;

import java.util.Random;

public class RandomMessageLengthGenerator
{
    private final Random random = new Random();

    public MessageLength newRandomMessageLength()
    {
        MessageLength[] allMessageLengths = MessageLength.values();

        int ix = random.nextInt(allMessageLengths.length);

        return allMessageLengths[ix];
    }
}
