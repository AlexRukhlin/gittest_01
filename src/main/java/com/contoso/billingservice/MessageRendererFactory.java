package com.contoso.billingservice;

public interface MessageRendererFactory
{
    MessageRenderer getDefaultMessageRenderer();
}
