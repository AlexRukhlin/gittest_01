package com.contoso.billingservice;

public class Main
{

    public static void main(String[] args)
    {
        BillingServiceMessageFactory messageFactory = new BillingServiceMessageFactoryImpl();

        RandomMessageLengthGenerator messageLengthGenerator = new RandomMessageLengthGenerator();
        MessageLength messageLength = messageLengthGenerator.newRandomMessageLength();

        String message = messageFactory.getHelloWorldMessage(messageLength);

        MessageRendererFactory messageRendererFactory = new MessageRendererFactoryImpl();

        MessageRenderer messageRenderer = messageRendererFactory.getDefaultMessageRenderer();

        messageRenderer.renderMessage(message);

    }
}
