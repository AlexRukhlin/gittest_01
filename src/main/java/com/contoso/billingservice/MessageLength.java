package com.contoso.billingservice;

/**
 * MessageLength enumberation.
 */
public class MessageLength
{
	private final int lengthType;
	public static MessageLength SHORT = new MessageLength(0);
	public static MessageLength MEDIUM = new MessageLength(1);
	public static MessageLength LONG = new MessageLength(2);
	public static MessageLength VERBOSE = new MessageLength(3);
	
	private MessageLength(int lengthType)
	{
		this.lengthType = lengthType;
	}
	
	public static MessageLength[] values()
	{
		return new MessageLength[] {MessageLength.SHORT, MessageLength.MEDIUM, MessageLength.LONG, MessageLength.VERBOSE };
	}
	
}
