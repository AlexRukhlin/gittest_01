package com.contoso.billingservice;

/**
 * Implementation of {@link MessageRenderer} that simply outputs results to STDOUT
 */
public class ConsoleMessageRenderer
    implements MessageRenderer
{
    public void renderMessage(String message)
    {
        if (message == null)
        {
            throw new IllegalArgumentException("message can't be null");
        }

        System.out.println(message);
    }
}
