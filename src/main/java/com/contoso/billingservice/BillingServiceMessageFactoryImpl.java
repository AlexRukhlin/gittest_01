package com.contoso.billingservice;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of {@link BillingServiceMessageFactory} that has basic Hello World messages.
 */
public class BillingServiceMessageFactoryImpl implements BillingServiceMessageFactory
{
    private final Map messages = new HashMap();

    public BillingServiceMessageFactoryImpl()
    {
        messages.put(MessageLength.SHORT, "hi");
        messages.put(MessageLength.MEDIUM, "hello world");
        messages.put(MessageLength.LONG, "Hello, World!");
        messages.put(MessageLength.VERBOSE, "Hello there, have a nice day!");
    }

    /* (non-Javadoc)
     * @see com.example.helloworld.HelloWorldMessageFactory#getHelloWorldMessage(com.example.helloworld.MessageLength)
     */
    public String getHelloWorldMessage(MessageLength length)
    {
        if (length == null)
        {
            throw new IllegalArgumentException("length must not be null");
        }

        String message = messages.get(length).toString();

        if (message == null)
        {
            throw new IllegalArgumentException("no message available for length [" + length + "]");
        }

        return message;
    }
}
