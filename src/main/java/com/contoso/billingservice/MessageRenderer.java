package com.contoso.billingservice;

/**
 * Interface to be implemented by all MessageRenders
 */
public interface MessageRenderer
{
	/**
	 * Render the passed message.
	 * @param message
	 */
    void renderMessage(String message);
}
