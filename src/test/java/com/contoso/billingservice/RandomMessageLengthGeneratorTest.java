package com.contoso.billingservice;

import junit.framework.TestCase;

public class RandomMessageLengthGeneratorTest extends TestCase {

	public void testNewRandomMessageLength() {
		
		RandomMessageLengthGenerator generator = new RandomMessageLengthGenerator();
		
		int numShort = 0;
		int numMedium = 0;
		int numLong = 0;
		int numVerbose = 0;
		
		// Let's use a large test size so this test takes
		// some time to run.
		int testSize = 1000000;
		for (int i = 0; i < testSize; i++) 
		{
			MessageLength random = generator.newRandomMessageLength();
			if (MessageLength.SHORT.equals(random))
			{
				numShort ++;
			}
			else if (MessageLength.MEDIUM.equals(random))
			{
				numMedium ++;
			}
			else if (MessageLength.LONG.equals(random))
			{
				numLong ++;
			}
			else if (MessageLength.VERBOSE.equals(random))
			{
				numVerbose ++;
			}
			else
			{
				fail("An unkown MessageLength was returned");
			}
		}
		
		assertTrue(numLong + numMedium + numShort + numVerbose == testSize);
		assertTrue("No length of type MessageLength.Short were returned", numShort > 0);
		assertTrue("No length of type MessageLength.Medium were returned", numMedium > 0);
		assertTrue("No length of type MessageLength.Long were returned", numLong > 0);
		assertTrue("No length of type MessageLength.Verbose were returned", numVerbose > 0);
		
	}

}
