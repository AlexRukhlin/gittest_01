package com.contoso.billingservice;

import junit.framework.TestCase;

public class ConsoleMessageRendererTest extends TestCase {

	public void testRenderMessage() 
	{
		ConsoleMessageRenderer renderer = new ConsoleMessageRenderer();
		renderer.renderMessage("Hello World!");
		assertTrue(true);
	}
	
	public void testRenderNullMessage() 
	{
		ConsoleMessageRenderer renderer = new ConsoleMessageRenderer();
		boolean errorDetected = false;
		
		try
		{
		renderer.renderMessage(null);
		}
		catch (IllegalArgumentException e) {
			errorDetected = true;
		}
		
		assertTrue("IllegalArgumentException expected when renderMessage called with null", errorDetected);
	}

}
