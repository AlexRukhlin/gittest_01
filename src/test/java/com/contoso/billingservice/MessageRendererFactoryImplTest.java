package com.contoso.billingservice;
import com.contoso.billingservice.MessageRendererFactoryImpl;

import junit.framework.TestCase;

/**
 * 
 * @author martin
 *
 */
public class MessageRendererFactoryImplTest
    extends TestCase
{
    private com.contoso.billingservice.MessageRendererFactory messageRenderer;

    protected void setUp()
        throws Exception
    {
        super.setUp();
        messageRenderer = new MessageRendererFactoryImpl();
    }

    public void testNonNull()
    {
        assertNotNull(messageRenderer.getDefaultMessageRenderer());
    }
}
