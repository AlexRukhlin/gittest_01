package com.contoso.billingservice;
import junit.framework.TestCase;

import com.contoso.billingservice.BillingServiceMessageFactory;
import com.contoso.billingservice.BillingServiceMessageFactoryImpl;
import com.contoso.billingservice.MessageLength;

public class BillingServiceMessageFactoryImplTest
    extends TestCase
{
    private BillingServiceMessageFactory messageFactory;

    protected void setUp()
        throws Exception
    {
        super.setUp();
        messageFactory = new BillingServiceMessageFactoryImpl();
    }

    public void testNonNullMessages()
    {
        MessageLength[] lengths = MessageLength.values();
        for (int i = 0; i < lengths.length; i++)
        {
            assertNotNull(messageFactory.getHelloWorldMessage(lengths[i]));
        }
    }

    public void testMessageLengths()
    {
        assertTrue(messageFactory.getHelloWorldMessage(MessageLength.SHORT).length() < messageFactory.getHelloWorldMessage(
            MessageLength.MEDIUM).length());
        assertTrue(messageFactory.getHelloWorldMessage(MessageLength.MEDIUM).length() < messageFactory.getHelloWorldMessage(
            MessageLength.LONG).length());
        assertTrue(messageFactory.getHelloWorldMessage(MessageLength.LONG).length() < messageFactory.getHelloWorldMessage(
            MessageLength.VERBOSE).length());
    }

    public void testFail()
    {
        assertFalse("Deliberately fail test", false);
    }
    
    public void testNullLengthThrows()
    {
        try
        {
            messageFactory.getHelloWorldMessage(null);
            fail();
        }
        catch (IllegalArgumentException ex)
        {

        }
    }
}
